create procedure CK.sStoredProcedureInputOutput
	@p1 int,
	@p2 tinyint/*not null*/= 0,
	@p3 smallint /*not null*/ output,
	@p4 nvarchar(50)=N'Murfn...',
	@p5 varchar(max) output /*input*/,	-- ==>  SqlParameterDirection == InputOutput
	@p6 char /*not null input*/ output,	-- ==>  SqlParameterDirection == InputOutput
	@p7 Xml output, -- input			   ==> this DOES NOT set SqlParameterDirection to be InputOutput since 
										    -- the comma is the list item separator.
	-- The "i n p u t" above will make @p8 an input/output
	@p8 smalldatetime output,
	-- null default
	@p9 smalldatetime = null
as
begin
	return 0;
end
